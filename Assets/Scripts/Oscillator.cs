﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
   [SerializeField] Vector3 movementVector = new Vector3(5f, 0f, 0f);
   [SerializeField] float period = 2f;

   [Range(0,1)] [SerializeField] float movementFactor;

    Vector3 startingPos;

    void Start()
    {
        startingPos = transform.position;
    }

    
    void Update()
    {
        if (period <= Mathf.Epsilon) { return; }
        float cycles = Time.time / period; //число циклов

        const float tau = Mathf.PI * 2; //около 6.28
        float rawSinWave = Mathf.Sin(cycles * tau); // от -1 до +1

        movementFactor = rawSinWave / 2f + 0.5f;
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
    }
}
